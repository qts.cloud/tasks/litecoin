# Task Description

* Docker-ayes -> [SOLVED HERE](build/Dockerfile)
  * Write a Dockerfile to run Litecoin 0.18.1 in a container.
    * It should somehow verify the checksum of the downloaded release (there's no need to build the project)
    * run as a normal user
    * when run without any modifiers (i.e. `docker run somerepo/litecoin:0.18.1`) should run the daemon and print its output to the console.
    * The build should be security conscious (and ideally pass a container image security test such as Anchore).
* k8s FTW: -> [SOLVED HERE](helm/app/templates/statefulset.yaml)
  * Write a Kubernetes StatefulSet to run the above, using persistent volume claims and resource limits.
* All the continuouses -> [SOLVED HERE](.gitlab-ci.yml)
  * Write a simple build and deployment pipeline for the above using Groovy/Jenkinsfile, Travis CI or Gitlab CI.
* Script kiddies
  * Source or come up with a text manipulation problem and solve it with at least two of awk, sed, tr and / or grep.
  * Check the question below first though, maybe.

    ```sh
    cat <<EOF > artifacts.env
    IMAGE_TAG=ibacalu/litecoin:4e093538
    CHART_VERSION=1.0.0
    APP_VERSION=4e093538
    EOF

    # silly, i know
    export DOCKERHUB_USER=$(cat artifacts.env | grep IMAGE_TAG | tr '[=:/]' ' ' | awk '{print $2}')
    ```

* Script grown-ups
  * Solve the problem in question 4 using any programming language you like.

    ```py
    import re
    PATTERN = r'IMAGE_TAG=(.+)/(.*):.*'

    def gen_tag(file_path, postfix = "cache", pattern = PATTERN):
      '''
        Generating valid docker image tag for kaniko caching
        Usage:
          gen_tag("artifacts.env")
        Returns:
          repo/image_name-cache
      '''

      file = open(file_path).read()
      original_tag = re.findall(PATTERN, file)[0]
      cache_tag = "/".join(original_tag) + "-{}".format(postfix)
      return cache_tag


    tag = gen_tag("artifacts.env")
    print("Generated tag: {}".format(tag))
    ```

* Terraform lovers unite -> [SOLVED HERE](terraform/README.md)
  * write a Terraform module that creates the following resources in IAM
    * A role, with no permissions, which can be assumed by users within the same account,
    * A policy, allowing users / entities to assume the above role,
    * A group, with the above policy attached,
    * A user, belonging to the above group.
  * All four entities should have the same name, or be similarly named in some meaningful way given the context e.g.:
    * prod-ci-role
    * prod-ci-policy
    * prod-ci-group
    * prod-ci-user
    * or just prod-ci
  * Make the suffixes toggleable, if you like.
