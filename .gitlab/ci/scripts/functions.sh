#!/bin/bash

colorize() {
  export RED="\033[31m"
  export GREEN="\033[92m"
  export VIVID="\033[95m"
  export RST="\033[0m"
}

log_info() {
  echo -e "${GREEN}[INFO]: $1${RST}"
}

log_warning() {
  echo -e "${YELLOW}[WARNING]: $1${RST}"
}

log_error() {
  echo -e "${RED}[ERROR]: $1${RST}"
}

log_fatal() {
  echo -e "${RED}[FATAL]: $1${RST}"
  return 1
}

log_section() {
  echo -e "section_start:$(date +%s):section_$(date +%s)\r\e[0K${VIVID}$1${RST}"
}

check_variables_defined() {
  for var in "$@"; do
    if [ -z "$(eval "echo \$$var")" ]; then
      log_fatal "${var} not defined";
    fi
  done
}

colorize