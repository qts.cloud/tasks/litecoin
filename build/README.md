# Docker-ayes

## Task

Write a Dockerfile to run Litecoin 0.18.1 in a container. It should somehow verify the
checksum of the downloaded release (there's no need to build the project), run as a normal user, and
when run without any modifiers (i.e. docker run somerepo/litecoin:0.18.1) should run the daemon,
and print its output to the console. The build should be security conscious (and ideally pass a
container image security test such as Anchore).

## Description

### Build the image

```sh
#? Docker build image
docker build --build-arg LITECOIN_VERSION=0.18.1 --build-arg LITECOIN_GPG_PUBLIC_KEY="0xfe3348877809386c" -t ibacalu/litecoin:0.18.1 .

#? alternative docker-compose build
docker-compose build
```

### Push the image to registry

```sh
docker push ibacalu/litecoin:0.18.1
```

### Docker Scan (trivy)

```sh
#? Install prequisites
brew install trivy

#? Scan Dockerfile
trivy conf --severity HIGH Dockerfile

#? Scan Built Image
trivy image --severity HIGH litecoin:0.18.1
```

### Run image

```sh
docker-compose up
```

### Conclusions

Image has been tested and from my perspective seems to be working as required.
At this point I would consult with the subject-matter experts and QA in order to really test out if everything is according to the requirements.
