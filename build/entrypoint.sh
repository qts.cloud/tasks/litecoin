#!/bin/bash

set -eu

#? Env Variables
export WORKDIR=${WORKDIR:-"/litecoin"}


#? Perhaps some other configuration would be required for litecoin?
if [ ! -s "${WORKDIR}/.litecoin/litecoin.conf" ]; then
	cat <<-EOF > "${WORKDIR}/.litecoin/litecoin.conf"
	printtoconsole=1
	rpcuser=litecoin
	rpcpassword=password
	rpcbind=0.0.0.0
	rpcallowip=0.0.0.0
	rpcport=9332
	EOF
fi

exec "$@"