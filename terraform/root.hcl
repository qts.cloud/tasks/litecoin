locals {
  name   = basename(get_terragrunt_dir())
  path   = get_terragrunt_dir()
  region = "eu-central-1"
}

generate "root_provider" {
  path      = "root.providers.tf"
  if_exists = "overwrite"
  contents  = <<-EOF
    provider "aws" {
      region  = "${local.region}"
    }
EOF
}

remote_state {
  backend = "local"
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite"
  }
  config = {
    path = "${get_terragrunt_dir()}/terraform.tfstate"
  }
}

#? Don't run plans for this
skip = true
