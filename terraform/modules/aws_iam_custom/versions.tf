terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.6.0"
    }
  }
  required_version = ">= 1.1.4"
  experiments      = [module_variable_optional_attrs]
}