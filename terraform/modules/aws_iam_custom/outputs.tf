output "aws_iam_role" {
  description = "aws_iam_role resource"
  value       = aws_iam_role.this
}

output "aws_iam_group" {
  description = "aws_iam_group resource"
  value       = aws_iam_group.this
}

output "aws_iam_group_policy" {
  description = "aws_iam_group_policy resource"
  value       = aws_iam_group_policy.this
}

output "aws_iam_access_key" {
  description = "AWS_ACCESS_KEY_ID and encrypted, base64 encoded AWS_SECRET_ACCESS_KEY."
  value = {
    AWS_ACCESS_KEY_ID     = try(aws_iam_access_key.this.0.id, null)
    AWS_SECRET_ACCESS_KEY = try(aws_iam_access_key.this.0.encrypted_secret, null)
  }
}

output "aws_iam_user_login_profile" {
  description = "Encrypted, base64 encoded user password."
  value = {
    AWS_URL       = "https://${data.aws_caller_identity.this.account_id}.signin.aws.amazon.com/console"
    IAM_USER_NAME = try(aws_iam_user_login_profile.this.0.user, null)
    IAM_USER_PASS = try(aws_iam_user_login_profile.this.0.encrypted_password, null)
  }
}

output "aws_credentials" {
  description = "Detailed credentials output"
  value       = local.pgp_key != null ? local.aws_credentials : null
}