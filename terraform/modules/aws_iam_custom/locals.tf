locals {
  environment = try(coalesce(var.config.environment), "dev")
  path        = try(coalesce(var.config.path), "/")
  tags = merge(
    try(coalesce(var.config.tags), {}),
    {
      Environment = local.environment
      Terraform   = "true"
    }
  )
  pgp_key = try(coalesce(var.config.pgp_key), null)


  #? Generate output statement for easy credential extraction
  aws_credentials = <<EOF
  # If pgp_key was provided, then these values were generated for you (assuming keybase):

  # Access UI
  AWS_URL=https://${data.aws_caller_identity.this.account_id}.signin.aws.amazon.com/console
  IAM_USER_NAME=${try(aws_iam_user_login_profile.this.0.user, null)}
  IAM_USER_PASS=$(echo "${try(aws_iam_user_login_profile.this.0.encrypted_password, null)}" | base64 -d | keybase pgp decrypt)

  # Access Programatically
  AWS_ACCESS_KEY_ID=${try(aws_iam_access_key.this.0.id, null)}
  AWS_SECRET_ACCESS_KEY=$(echo "${try(aws_iam_access_key.this.0.encrypted_secret, null)}" | base64 -d | keybase pgp decrypt)
  EOF
}
