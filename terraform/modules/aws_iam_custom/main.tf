resource "aws_iam_role" "this" {
  name        = "${local.environment}-ci-role"
  path        = local.path
  description = "AWS IAM Role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          AWS = "arn:aws:iam::${data.aws_caller_identity.this.account_id}:root"
        }
      },
    ]
  })
  tags = local.tags
}

resource "aws_iam_group" "this" {
  name = "${local.environment}-ci-group"
  path = local.path
}

resource "aws_iam_group_policy" "this" {
  name  = "${local.environment}-ci-group-policy"
  group = aws_iam_group.this.name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action   = ["sts:AssumeRole"]
        Effect   = "Allow"
        Resource = "arn:aws:iam::${data.aws_caller_identity.this.account_id}:role/${aws_iam_role.this.name}"
      }
    ]
  })
}

resource "aws_iam_user" "this" {
  name          = "${local.environment}-ci-user"
  path          = local.path
  force_destroy = true
  tags          = local.tags
}

resource "aws_iam_user_policy" "this" {
  name = "${local.environment}-ci-user-policy"
  user = aws_iam_user.this.name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "iam:GetAccountPasswordPolicy",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action = [
          "iam:ChangePassword",
        ]
        Effect   = "Allow"
        Resource = "arn:aws:iam::${data.aws_caller_identity.this.account_id}:user/$${aws:username}"
      },
    ]
  })
}

resource "aws_iam_user_group_membership" "this" {
  user = aws_iam_user.this.name

  groups = [
    aws_iam_group.this.name
  ]
}

resource "aws_iam_access_key" "this" {
  count = local.pgp_key != null ? 1 : 0

  user    = aws_iam_user.this.name
  pgp_key = local.pgp_key
}

resource "aws_iam_user_login_profile" "this" {
  count = local.pgp_key != null ? 1 : 0

  user                    = aws_iam_user.this.name
  pgp_key                 = local.pgp_key
  password_reset_required = true

  lifecycle {
    ignore_changes = [
      password_reset_required
    ]
  }
}