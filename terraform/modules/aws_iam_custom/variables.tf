variable "config" {
  description = "(Required) Module configuration"
  type = object({
    environment = string,
    path        = optional(string),
    tags        = optional(map(string)),
    # (Optional) Either a base-64 encoded PGP public key, or a keybase username in the form keybase:some_person_that_exists, for use in the encrypted_secret output attribute.
    pgp_key = optional(string),
  })
}
