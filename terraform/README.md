# Terraform & Terragrunt

## Using [Terragrunt](https://terragrunt.gruntwork.io)

Benefits:

- It makes use of directory tree and the `environment` folder name itself to deploy resources/keep state
- Using a clean syntax and inheritance model, one could achieve very complex setups while maintaining the structure simple and easy to understand.
- Separation of concerns for terraform state and CI runs.

`root.hcl`:

- central `Terragrunt` configuration file.
- stores configuration for dynamic `state` and `aws provider` configuration
- is included in all resources.

`terragrunt.hcl`:

- module configuration.
- generates required `terraform` module inputs
- includes `root.hcl` configuration
- can be used to setup resource dependency trees(wasn't required for this test).

## How to ?

1. Use the module in terraform

    ```hcl
    module "aws_iam_custom" {
      source = "git::https://gitlab.com/qts.cloud/tasks/litecoin.git//terraform/modules/aws_iam_custom?ref=v1.2.0""

      config = {
        environment = "development"
        pgp_key     = "keybase:ibacalu"
      }
    }
    ```

1. Requirements

    ```sh
    # Install Terragrunt (https://terragrunt.gruntwork.io)
    brew install terragrunt

    # [AWS Credentials](https://terragrunt.gruntwork.io/docs/features/work-with-multiple-aws-accounts/)
    # There are multiple ways of how you could use Terragrunt to connect to different AWS Accounts. The simplest one is to just export required ENV VARS
    export AWS_ACCESS_KEY_ID=my-key-id
    export AWS_SECRET_ACCESS_KEY=my-secret-key-id

    # NOTE: In a normal work environment, Terragrunt should be configured to use different IAM Roles and different AWS Accounts to maintain separation of concerns and a decent security gap between accounts. It could be even possible to dynamically auto-configure different state destinations and IAM Roles based on folder structure.
    ```

1. Automatic Deployment:

    ```sh
    # must be in folder: tests/
    terragrunt run-all plan
    terragrunt run-all apply
    terragrunt run-all destroy
    ```

1. Individual Deployment:
    1. Deploy `tests/dev`

        ```sh
        cd tests/dev
        terragrunt plan
        terragrunt apply

        #? Retrieve credentials
        terragrunt output -raw aws_credentials

        #? Destroy
        terragrunt destroy
        ```

    1. Deploy `tests/stage`

        ```sh
        cd tests/stage
        terragrunt plan
        terragrunt apply

        #? Retrieve credentials
        terragrunt output -raw aws_credentials

        #? Destroy
        terragrunt destroy
        ```

    1. Deploy `tests/prod`

        ```sh
        cd tests/prod
        terragrunt plan
        terragrunt apply

        #? Retrieve credentials
        terragrunt output -raw aws_credentials

        #? Destroy
        terragrunt destroy
        ```
