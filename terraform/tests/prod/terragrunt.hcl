include "root" {
  path   = find_in_parent_folders("root.hcl")
  expose = true
}

terraform {
  source = "${get_parent_terragrunt_dir()}/modules//aws_iam_custom"
}

locals {
  stage = basename(get_terragrunt_dir())
}

inputs = {
  config = {
    environment = local.stage
    pgp_key     = "keybase:ibacalu"
  }
}
